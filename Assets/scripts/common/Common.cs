﻿using UnityEngine;
using KBEngine;
using System.Collections;

public class Common {
	public static bool isDebugBuild = false;
	public static LayerMask moveIgonreLayerMask = ~(1 << LayerMask.NameToLayer("kbentity"));
	
	public static string safe_url(string path)
	{
#if UNITY_WEBPLAYER
		return KBEngineApp.url + path;
#else
		return "file://" + Application.dataPath + path;
#endif
	}
	
	public string getPlatformDefines() 
	{
#if UNITY_EDITOR
		return "Unity Editor";
#endif

#if UNITY_IPHONE
		return "Iphone";
#endif

#if UNITY_STANDALONE_OSX
		return "Stand Alone OSX";
#endif

#if UNITY_STANDALONE_WIN
		return "Stand Alone Windows";
#endif
		
#if UNITY_WEBPLAYER
		return "webplayer";
#endif
	}
	
	public static void DEBUG_MSG(object s)
	{
		if(isDebugBuild == false)
			return;
		
#if UNITY_WEBPLAYER
		Application.ExternalCall("console.log", s);
#endif
		Debug.Log("[DEBUG]:" + s);
	}
	
	public static void WARNING_MSG(object s)
	{
#if UNITY_WEBPLAYER
		Application.ExternalCall("console.log", s);
#endif
		Debug.LogWarning("[WARNING]:" + s);
	}
	
	public static void ERROR_MSG(object s)
	{
#if UNITY_WEBPLAYER
		Application.ExternalCall("console.log", s);
#endif
		Debug.LogError("[ERROR]:" + s);
	}
	
	public static float calcPositionY(Vector3 pos)
	{
		RaycastHit hit = new RaycastHit();
		Vector3 offset = new Vector3(0, 1f, 0);
		
		if (Physics.Raycast (pos + offset, -Vector3.up, out hit, 2f, moveIgonreLayerMask)) 
		{
			return hit.point.y;
		}
		
		return pos.y;
	}
}
